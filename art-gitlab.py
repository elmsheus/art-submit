#!/usr/bin/env python3
# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
"""
ART-gitlab - ATLAS Release Tester (submit command).

Usage:
  art-gitlab.py included            [-v]  <nightly_release> <project> <platform>
  art-gitlab.py wait_for            [-v --no-wait]  <nightly_release> <project> <platform> <nightly_tag>
  art-gitlab.py packages            [-v]  <nightly_release> <project> <platform> [<type>]

Options:
  -h --help         Show this screen.
  --no-wait         Do not wait an extra 30 minutes after finding the logfile of the release
  --version         Show version.
  -v, --verbose     Show details.

Sub-commands:
  included          Check if a release and platform is included
  wait_for          Wait for the release to be available
  packages          Returns 0 and 1 to abort. An optional list of packages is printed to stdout

Arguments:
  nightly_release   Name of the nightly release (e.g. 21.0)
  nightly_tag       Nightly tag (e.g. 2017-02-26T2119)
  platform          Platform (e.g. x86_64-slc6-gcc62-opt)
  project           Name of the project (e.g. Athena)
  type              local or grid
"""
from __future__ import print_function

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"
__version__ = '1.4.12'

import fnmatch
import glob
import os
import sys
import time
import yaml

from docopt_dispatch import dispatch

cvmfs_directory = '/cvmfs/atlas-nightlies.cern.ch/repo/sw'


def count_string_occurrence(path, string):
    """Count number of occurences of 'string' inside latest file on 'path'. Returns count or 0."""
    files = sorted(glob.iglob(path), key=os.path.getctime, reverse=True)
    if not files:
        return 0

    print(files[0])
    sys.stdout.flush()
    f = open(files[0])
    contents = f.read()
    f.close()
    return contents.count(string)


def get_config():
    """Retrieve dictionary of ART configuration file."""
    config_file = open("art-configuration.yml", "r")
    config = yaml.safe_load(config_file)
    config_file.close()
    return config


@dispatch.on('included')
def included(nightly_release, project, platform, **kwargs):
    """TBD."""
    platform_pattern = '*-*-*-opt'
    if not fnmatch.fnmatch(platform, platform_pattern):
        print('Excluding ' + 'all' + ' for ' + nightly_release + ' project ' + project + ' on ' + platform)
        print('art-status: excluded')
        exit(1)

    if nightly_release == 'const-master' and project == 'Athena':
        print('Excluding ' + 'all' + ' for ' + nightly_release + ' project ' + project + ' on ' + platform)
        print('art-status: excluded')
        exit(1)

    print('art-status: included')
    exit(0)


@dispatch.on('wait_for')
def wait_for(nightly_release, project, platform, nightly_tag, **kwargs):
    """TBD."""
    look_for = "looks to have been successful"
    if nightly_release not in ['master--dev4LCG']:
        # look and wait for ayum file
        directory = os.path.join(cvmfs_directory, "_".join((nightly_release, project, platform)), nightly_tag)
        path = os.path.join(directory, nightly_release + "__" + project + "__" + platform + "*" + nightly_tag + "__*.ayum.log")

        count = 0
        needed = 1
        value = count_string_occurrence(path, look_for)
        print("art-status: waiting")
        print(path)
        print("count: " + str(value) + " mins: " + str(count))
        sys.stdout.flush()
        # 90 minutes wait
        while (value < needed) and (count < 90):
            time.sleep(60)
            count += 1
            value = count_string_occurrence(path, look_for)
            print("count: " + str(value) + " mins: " + str(count))
            sys.stdout.flush()

        if value < needed:
            print("art-status: no release")
            sys.stdout.flush()
            exit(2)

    no_wait = kwargs['no_wait']
    if not no_wait:
        wait_time = 30
        print("Waiting an extra", wait_time, "mins for the release to be delivered to remote sites")
        sys.stdout.flush()
        time.sleep(wait_time * 60)

    print("art-status: setup")
    sys.stdout.flush()

    exit(0)


def matched(entry, nightly_release, project, platform):
    """TBD."""
    if 'branches' in entry:
        for branch in entry['branches']:
            if nightly_release == branch:
                if 'projects' in entry:
                    for p in entry['projects']:
                        if project == p:
                            # Branch and Project match
                            return True
                else:
                    # Branch matched, no projects
                    return True
    else:
        # No branches
        if 'projects' in entry:
            for p in entry['projects']:
                if project == p:
                    # No branches, project matched
                    return True
    # No branches, no projects
    return False


@dispatch.on('packages')
def packages(nightly_release, project, platform, type=None, **kwargs):
    """TBD."""
    yml_file = os.path.join('.', 'art-submit.yml')
    if os.path.isfile(yml_file):
        with open(yml_file, 'r') as stream:
            try:
                config = yaml.safe_load(stream)
                for entry in config:
                    if matched(entry, nightly_release, project, platform):
                        if 'runs_on' in entry:
                            if not type:
                                # needs to run, but do not know where
                                # normally called from an early stage in gitlab
                                # may be excluded later on when type is known
                                exit(0)
                            elif type in entry['runs_on']:
                                # run these with specified packages if any
                                if 'packages' in entry:
                                    print(' '.join((entry['packages'])))
                                exit(1)
                            else:
                                # not in demanded type
                                print('Excluding ' + type + ' for ' + nightly_release + ' project ' + project + ' on ' + platform)
                                print('art-status: excluded')
                                exit(2)
                        else:
                            # no type specified, no need to run
                            print('Excluding ' + 'local/grid' + ' for ' + nightly_release + ' project ' + project + ' on ' + platform)
                            print('art-status: excluded')
                            exit(2)
            except yaml.YAMLError as exc:
                print(exc)
                exit(2)
    exit(0)


if __name__ == '__main__':
    dispatch(__doc__, version=os.path.splitext(os.path.basename(__file__))[0] + ' ' + __version__)
